const html = require('nanohtml')
const raw = require('nanohtml/raw')
const md = require('markdown-it')()

var colors = ['var(--bg-one)', 'var(--bg-two)', 'var(--bg-three)', 'var(--bg-four)', 'var(--big-five)']
var index = 0

module.exports = view

function view (state, emit) {
  if (state.text.paragraphs) {
    return html`${renderParagraphs(state.text.paragraphs)}`
  } else {
    return html`<h1>loading</h1>`
  }

function renderParagraphs (paragraphs) {
    return paragraphs.map(paragraph => {
        randomizeColors()
        var content = raw(md.render(paragraph))
        return html`
        <section class='text' style='background: ${colors[index]}'>
          ${content}
        </section>`
    })
  }

function randomizeColors () {
    return (index === colors.length) ? index = 0 : index += 1
}
}

# T E X T 
  
This is the gooooods, baby!  This is the text that makes up the heart of your zine.

These are the contents, is what I am saying.  _THE JUICE_.

I am sorry.

# Adding your words!

So to make the zine filled with yr own words, simply delete the .txt file you see in here and replace it with your own.  I mean this literally, the file you put in here should be called `my-good-words.txt` or `my-story.txt` or what haves.

This .txt file should be formatted in this way:

```
title: my good words
----
text:

all the text you wanna put in and all that good stuff.
```

This format is a bit important, so make sure yr file looks like this one, it starts with the title:, then `----` then text:

## Adding multiple pages of words

To separate your text into multiple pages of zine, just add a twinkly stars as page breaks!

Like so:

```
title: my other good words
----
text:

I start with the first page.

And It can be as long as I want, and now

I want a second page.

~~*

And here is that second page. It can also be as long as I want until..

~~*

thiiiird page

```
That's it.  Like above, the characters are important, so you break up your pages with these characters specific: `~~*`

## Styling your Text with Markdown

LASTLY! You can style up your text with markdown.  This allows you to add in headers, bolded text, italics, and more.

So yr file would look like so:

```
title: my stylish good words
----
I want to **bold** font and I want to _italicize_ font.  also:
~~*
# This is a heading on the  next page

that was a heading!

```

You can read more about markdown here: [Scuttlebutt's Guide to Markdown](http://thesolarpunk.club/guides/markdown/index.html)

const html = require('nanohtml')

const cover = require('./cover')
const text = require('./text')
const colophon = require('./colophon')
const backCover = require('./backCover')

module.exports = (state, emit) => {
  return html`
  <body>
    ${cover(state, emit)}
    ${text(state, emit)}
    ${colophon(state, emit)}
    ${backCover(state, emit)}
  </body>
  `
}

// Bring in our outdoor modules
const choo = require('choo')

// Bring in our indoor modules
const cover = require('./paper/cover')
const text = require('./paper/text')
const colophon = require('./paper/colophon')
const assembledZine = require('./paper/assembledZine')

// Initialize choo
const app = choo()

app.use(require('choo-devtools')())

app.use(require('./binding/assembleZine'))
app.use(require('./binding/textNavigation'))

app.mount('body')

app.route('/', assembledZine)

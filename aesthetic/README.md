# A E S T H E T I C

This folder contains the images, assets, and CSS Stylesheets for your zine.  It's where you can adjust the code to match yr own personal aesthetic.

You don't actually need to know CSS to make general customization's to yr zine's style, it's only when you wanna get deep into it's looks.

# Folder Structure

- **Assets/** - holds the icons for the site (the previous and next arrow).  This is also a good folder to use when putting in extra images.
- **cover-image**/ - Holds the cover image.  It can be named whatever you'd like.
- **fonts/**- holds custom fonts you've downloaded to reference and use.  This is where our [mononoki](https://madmalik.github.io/mononoki/) font files live.
- **Stylesheets/** The heart of this folder, and we'll go into more deets below!

# The Stylesheets

There's two stylesheets, one that can be used without code knowledge and the other that requires some CSS knowledge.

To make geneeral changes, check out `colors-and-fonts.css`.  This lets you change the colors(and fonts)  used throughout the program.  To do so, just click the edit pencil in Beaker, find the name of the thing whose color you wanna change, and then change it's current value.

you can use a [hex code](http://www.color-hex.com/) for the color, or use [web colors](https://en.wikipedia.org/wiki/Web_colors)

So if you wanna change the main background color from white to a soft aliceblue, you'd write it like this:
`--primary-bg: aliceblue;`

To make bigger changes, use `main.css`.  

# CSS Tutorials

- [html dog guide to css](http://www.htmldog.com/guides/css/beginner/applyingcss/)
- [Mozilla's fantastic docs](https://developer.mozilla.org/en-US/docs/Learn/CSS)








*CSS stands for 'Cascading Style Sheets'.  It's a language that pairs with your HTML and declares the look and feel of your site. It's forgiving, and fun to learn, and there's some tutorials at the bottom of this readme.


